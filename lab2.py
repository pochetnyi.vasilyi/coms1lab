import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt


def print_matrix(matrix):
    for i in range(matrix.shape[0] - 1, -1, -1):
        np.set_printoptions(formatter={'int': '{:3}'.format})
        print(matrix[i, :])


# H = np.array([[0, 0, 6, 6, 6, 8, 8, 7],
#               [3, 4, 5, 3, 3, 2, 4, 2],
#               [0, 6, 4, 4, 4, 4, 3, 7],
#               [5, 5, 6, 7, 6, 4, 3, 2],
#               [1, 1, 0, -2, -1, 1, 2, 3],
#               [5, 6, 5, 6, 4, 0, 3, 4],
#               [3, 4, 5, 6, 7, 5, 6, 6],
#               [6, 6, 6, 4, 5, 2, 3, 1]], int)
#
# X = np.array([[5, 5, 5, 5, 5, 5, 5, 5],
#               [3, 3, 4, 6, 5, 6, 3, 4],
#               [2, 2, 3, 3, 4, 4, 5, 5],
#               [5, 5, 6, 3, 4, 6, 5, 4],
#               [0, 2, 0, 3, 0, 3, 3, 4],
#               [1, 2, 3, 4, 5, 3, 4, 2],
#               [0, 4, 5, 6, 2, 3, 3, 0],
#               [5, 3, 0, 5, 2, 4, 0, 3]], int)

H = np.array([[6, 7, 8, 8, 5, 5, 4, 4],
              [0, 4, 4, 0, 5, 4, 5, 3],
              [4, 0, 3, 4, 3, 0, 2, 3],
              [0, 4, 4, 5, 6, 7, 3, 4],
              [6, 4, 0, 5, 0, 6, 6, 9],
              [5, 4, 5, 6, 7, 4, 5, 6],
              [4, 5, 0, 3, 4, 5, 2, 4],
              [4, 6, 7, 4, 0, 5, 0, 0]], int)

X = np.array([[2, 2, 3, 3, 4, 4, 5, 5],
              [0, 3, 3, 3, 3, 4, 5, 5],
              [3, 3, 4, 4, 3, 4, 4, 4],
              [0, 0, 5, 0, 6, 5, 6, 5],
              [0, 3, 0, 0, 6, 7, 6, 7],
              [3, 4, 5, 0, 4, 5, 4, 5],
              [0, 6, 0, 6, 4, 2, 4, 3],
              [5, 0, 2, 4, 2, 3, 4, 5]], int)

H_ROWS, H_COLS = H.shape
X_ROWS, X_COLS = X.shape
Y_ROWS, Y_COLS = H_ROWS + X_ROWS - 1, H_COLS + X_COLS - 1

print('Входные данные:')
print('\nh(n1, n2) =')
print_matrix(H)
print('\nx(n1, n2) =')
print_matrix(X)

print('\nПоиск реакции системы y(n1, n2) с помощью дискретной свертки:')

Y = np.zeros((Y_ROWS, Y_COLS), int)
for (i, j), _ in np.ndenumerate(Y):
    for (k1, k2), x in np.ndenumerate(X):
        if (0 <= i - k1 < H_ROWS) and (0 <= j - k2 < H_COLS):
            h = H[i - k1][j - k2]
            Y[i][j] += x * h

print('\ny(n1, n2) =')
print_matrix(Y)

print('\nПоиск реакции системы y(n1, n2)\nпутем суммирования взвешенных и сдвинутых импульсных откликов:')

Y = np.zeros((Y_ROWS, Y_COLS), int)
for (k1, k2), x in np.ndenumerate(X):
    temp = np.zeros((Y_ROWS, Y_COLS), int)
    for (i, j), _ in np.ndenumerate(Y):
        if (0 <= i - k1 < H_ROWS) and (0 <= j - k2 < H_COLS):
            h = H[i - k1][j - k2]
            temp[i][j] = x * h
    Y += temp
    print('\nh(n1, n2) * x({}, {})'.format(k1, k2))
    print_matrix(temp)

print('\ny(n1, n2) =')
print_matrix(Y)

print('\nГрафическое представление выходного сигнала:')

N1 = np.arange(Y_ROWS)
N2 = np.arange(Y_COLS)
N1, N2 = np.meshgrid(N1, N2)

fig = plt.figure()

# ax = fig.gca(projection='3d')
ax = fig.add_subplot(projection='3d')
# plt.subplot(projection='3d')

ax.plot_surface(N1, N2, Y, cmap='coolwarm')
ax.set_xlabel('n1')
ax.set_ylabel('n2')
ax.set_zlabel('y')
plt.show()

fig, ax = plt.subplots()
ax.contour(N1, N2, Y, levels=10, linewidths=0.5, colors='k')
cntr = ax.contourf(N1, N2, Y, levels=14, cmap='RdBu_r')
ax.set_xlabel('n1')
ax.set_ylabel('n2')
fig.colorbar(cntr, ax=ax, label='y')
plt.show()
